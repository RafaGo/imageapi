package com.imageapi.imageapi.infra.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import static com.imageapi.imageapi.infra.repository.specs.GenericsSpec.conjunction;
import static com.imageapi.imageapi.infra.repository.specs.ImageSpecs.extensionEqual;
import static com.imageapi.imageapi.infra.repository.specs.ImageSpecs.nameLike;
import static com.imageapi.imageapi.infra.repository.specs.ImageSpecs.tagsLike;

import java.util.List;
import org.springframework.util.StringUtils;

import com.imageapi.imageapi.domain.entity.Image;
import com.imageapi.imageapi.domain.enums.ImageExtension;

public interface ImageRepository extends JpaRepository<Image, String>, JpaSpecificationExecutor<Image> {

   default List<Image> findByExtensionAndNameOrTargsLike(ImageExtension extension, String query) {
      Specification<Image> spec = Specification.where(conjunction());
      if (extension != null) {
         spec = spec.and(extensionEqual(extension));
      }
      if (StringUtils.hasText(query)) {
         spec = spec.and(Specification.anyOf(nameLike(query), tagsLike(query)));
      }
      return findAll(spec);
   }
}