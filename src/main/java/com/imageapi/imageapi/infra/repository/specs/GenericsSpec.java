package com.imageapi.imageapi.infra.repository.specs;

import org.springframework.data.jpa.domain.Specification;

public class GenericsSpec {

   private GenericsSpec() {
   }

   public static <T> Specification<T> conjunction() {
      return (root, q, criteriaBuilder) -> criteriaBuilder.conjunction();

   }
}
