package com.imageapi.imageapi.application.images;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.imageapi.imageapi.domain.entity.Image;
import com.imageapi.imageapi.domain.enums.ImageExtension;
import com.imageapi.imageapi.domain.service.ImageService;
import com.imageapi.imageapi.infra.repository.ImageRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
   private final ImageRepository repository;

   @Override
   @Transactional
   public Image save(Image image) {
      return repository.save(image);
   }

   @Override
   public Optional<Image> getById(String id) {
      return repository.findById(id);

   }

   @Override
   public List<Image> search(ImageExtension extension, String query) {
      return repository.findByExtensionAndNameOrTargsLike(extension, query);
   }

}
