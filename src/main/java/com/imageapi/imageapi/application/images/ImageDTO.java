package com.imageapi.imageapi.application.images;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ImageDTO {
   private String url;
   private String name;
   private Long size;
   private String extension;
   private LocalDate uploadDate;

}