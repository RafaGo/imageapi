package com.imageapi.imageapi.domain.service;

import java.util.Optional;

import com.imageapi.imageapi.domain.entity.Image;
import java.util.List;
import com.imageapi.imageapi.domain.enums.ImageExtension;

public interface ImageService {
   Image save(Image image);

   Optional<Image> getById(String id);

   List<Image> search(ImageExtension extension, String query);

}
